Wrath of the Storm God
   Version Number 1.2.2
   Version Date: 2/18/15

A computer game created entirely in Java.

Currently developing as a mobile phone (android) application.


![pic.png](https://bitbucket.org/repo/9naXLy/images/1726027230-pic.png)


The game is difficult, but beatable with a strategy.

*by John Banya*



**Links**

Timelapse: https://www.youtube.com/watch?v=v9UKaSPxrGc

----------------------------------------------------------------------------------------------------------------------------

**How to view source code:**

Source->Storm God->src-> jb/res

jb: Contains packages of java source files

res: Contains resource files (image files)


*Executable .jar file available in Downloads*